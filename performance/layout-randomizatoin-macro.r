#![feature(test)]
extern crate test;
use test::{Bencher, black_box};

macro_rules! memshift {
  () => {
    {
      let x = black_box(0);
      let x = black_box(x+1);
      //SHIFT_CODE
      black_box(x);
      // Silence 'unused variable' warning.
    }
  }
}

